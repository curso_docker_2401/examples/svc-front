import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import RegisterView from '../views/RegisterView.vue'
import AboutView from '../views/AboutView.vue'
import TasksList from '../components/TasksList.vue';

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    component: AboutView
  },
  {
    path: "/register",
    name: "register",
    component: RegisterView
  },
  {
    path: '/tasks',
    name: 'TasksList',
    component: TasksList,
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
