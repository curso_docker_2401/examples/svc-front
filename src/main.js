import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import * as Sentry from "@sentry/vue";

Sentry.init({
  App,
  dsn: "https://2dde5aa2fba84fb89acfcfe8765e6268@o305110.ingest.sentry.io/4505153434419200",
  integrations: [
    new Sentry.BrowserTracing({
      routingInstrumentation: Sentry.vueRouterInstrumentation(router),
    }),
    new Sentry.Replay(),
  ],
  // Performance Monitoring
  tracesSampleRate: 1.0, // Capture 100% of the transactions, reduce in production!
  // Session Replay
  replaysSessionSampleRate: 0.1, // This sets the sample rate at 10%. You may want to change it to 100% while in development and then sample at a lower rate in production.
  replaysOnErrorSampleRate: 1.0, // If you're not already sampling the entire session, change the sample rate to 100% when sampling sessions where errors occur.
});

createApp(App).use(router).mount('#app')
