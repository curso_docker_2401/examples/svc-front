# svc-front

Proyecto incompleto. Tenemos un Dockerfile que, en la línea 8, espera que el proyecto esté compilado en una carpeta dist.

Haciendo uso de `AS name` de la etiqueta `FROM` vamos a crear un [Multi-stage builds](https://docs.docker.com/build/building/multi-stage/).

Claves:
    - Selecciona una imagen base para la fase de build que te permita ejecutar los comandos de compilación.
    - Una vez generes la carpeta dist, continua con la configuración de nginx para servir los estáticos.


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
